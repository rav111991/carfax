package com.carfax;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.carfax.Model.CarDataModel;
import com.carfax.Model.Listings;
import com.carfax.adapter.CarDataAdapter;
import com.carfax.connections.APIClient;
import com.carfax.connections.ServerConnectionAPI;
import com.carfax.interfaces.OnListItemClickListener;
import com.carfax.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnListItemClickListener {

  @BindView(R.id.no_record_found)
  TextView noRecordFoundTextView;

  @BindView(R.id.car_list)
  android.support.v7.widget.RecyclerView carList;
  private ServerConnectionAPI apiInterface;
  private Listings[] carDataList;
  private int currentPosition;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    setUpView();
  }

  private void setUpView() {
    ButterKnife.bind(this);
    carList.setLayoutManager(new LinearLayoutManager(MainActivity.this));

    final ProgressDialog dialog = new ProgressDialog(MainActivity.this);
    dialog.setMessage("Fetching Data, please wait.");
    dialog.show();

    apiInterface = APIClient.getClient().create(ServerConnectionAPI.class);
    Call<CarDataModel> carDetailService = apiInterface.getCarDetail();
    carDetailService.enqueue(new Callback<CarDataModel>() {
      @Override
      public void onResponse(Call<CarDataModel> call, Response<CarDataModel> response) {
        if (dialog.isShowing()) {
          dialog.dismiss();
        }
        if (response != null && response.body() != null) {

          if (response.body().getListings() != null)

            carDataList = response.body().getListings();

          carList.setAdapter(new CarDataAdapter(response.body().getListings(), MainActivity.this));

        }

      }

      @Override
      public void onFailure(Call<CarDataModel> call, Throwable t) {
        noRecordFoundTextView.setVisibility(View.VISIBLE);
        carList.setVisibility(View.INVISIBLE);
        if (dialog.isShowing()) {
          dialog.dismiss();
        }
      }
    });
  }

  @Override
  public void itemClicked(View view, int position) {
    currentPosition = position;
    if (view instanceof Button) {
      if (carDataList[position].getDealer() != null && !Utilities.isStringEmpty(carDataList[position].getDealer().getPhone())) {
        requestPermission();

      } else {
        Toast.makeText(MainActivity.this, "Phone number not available!", Toast.LENGTH_SHORT).show();
      }

    } else {

      Intent carDetailActivityIntent = new Intent(MainActivity.this, CarDetailActivity.class);
      carDetailActivityIntent.putExtra("carDetail", carDataList[position]);
      startActivity(carDetailActivityIntent);
    }
  }

  private void requestPermission() {
    if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(MainActivity.this,
        Manifest.permission.CALL_PHONE)
        != PackageManager.PERMISSION_GRANTED) {
      Utilities.checkPermission(MainActivity.this);
    } else {
      Utilities.callDealer(carDataList[currentPosition].getDealer().getPhone(), MainActivity.this);

    }

  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String[] permissions, int[] grantResults) {
    switch (requestCode) {
      case Utilities.MY_PERMISSIONS_REQUEST_CALL: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // permission was granted,
          Utilities.callDealer(carDataList[currentPosition].getDealer().getPhone(), MainActivity.this);
        } else {
          // permission denied, boo! Disable the
          // functionality that depends on this permission.
        }
        return;
      }
    }
  }

  /**
   * method is used to make a call using number from server data
   */

}
