package com.carfax;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.carfax.Model.Listings;
import com.carfax.utils.Utilities;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarDetailActivity extends AppCompatActivity {

  @BindView(R.id.car_imageview)
  ImageView carImageview;
  @BindView(R.id.name_textview)
  TextView nameTextview;
  @BindView(R.id.price_textview)
  TextView priceTextview;
  @BindView(R.id.mileage_textview)
  TextView mileageTextview;
  @BindView(R.id.location_value)
  TextView locationValue;
  @BindView(R.id.color_value)
  TextView colorValue;
  @BindView(R.id.interior_color_value)
  TextView interiorColorValue;
  @BindView(R.id.drive_type_value)
  TextView driveTypeValue;
  @BindView(R.id.transmission_value)
  TextView transmissionValue;
  @BindView(R.id.body_style_value)
  TextView bodyStyleValue;
  @BindView(R.id.engine_value)
  TextView engineValue;
  @BindView(R.id.fuel_value)
  TextView fuelValue;
  @BindView(R.id.call_button)
  Button callButton;
  Listings listing;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_car_detail);
    listing = (Listings) getIntent().getSerializableExtra("carDetail");
    setUpView();

  }

  private void setUpView() {
    ButterKnife.bind(this);

    locationValue.setText(Utilities.getLocation(listing.getDealer()));
    nameTextview.setText(Utilities.getName(listing));
    if (!Utilities.isStringEmpty(listing.getMileage()))
      mileageTextview.setText(Utilities.changeToMiles(listing.getMileage()));

    if (!Utilities.isStringEmpty(listing.getCurrentPrice()))
      priceTextview.setText("$" + listing.getCurrentPrice());

    if (!Utilities.isStringEmpty(listing.getExteriorColor()))
      colorValue.setText(listing.getExteriorColor());

    if (!Utilities.isStringEmpty(listing.getInteriorColor()))
      interiorColorValue.setText(listing.getInteriorColor());

    if (!Utilities.isStringEmpty(listing.getDrivetype()))
      driveTypeValue.setText(listing.getDrivetype());

    if (!Utilities.isStringEmpty(listing.getTransmission()))
      transmissionValue.setText(listing.getTransmission());

    if (!Utilities.isStringEmpty(listing.getBodytype()))
      bodyStyleValue.setText(listing.getBodytype());

    if (!Utilities.isStringEmpty(listing.getEngine()))
      engineValue.setText(listing.getEngine());

    if (!Utilities.isStringEmpty(listing.getFuel()))
      fuelValue.setText(listing.getFuel());

    if (listing.getImages() != null && listing.getImages().getLarge().length > 0 && !Utilities.isStringEmpty(listing.getImages().getLarge()[0])) {
      String url = listing.getImages().getLarge()[0];
      Picasso.get().load(url).fit()
          .centerCrop().into(carImageview);
    }


    callButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        requestPermission();
      }
    });

  }

  private void requestPermission() {
    if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(CarDetailActivity.this,
        Manifest.permission.CALL_PHONE)
        != PackageManager.PERMISSION_GRANTED) {
      Utilities.checkPermission(CarDetailActivity.this);
    } else {
      Utilities.callDealer(listing.getDealer().getPhone(),CarDetailActivity.this);
    }

  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String[] permissions, int[] grantResults) {
    switch (requestCode) {
      case Utilities.MY_PERMISSIONS_REQUEST_CALL: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // permission was granted,
          Utilities.callDealer(listing.getDealer().getPhone(),CarDetailActivity.this);
        } else {
          // permission denied, boo! Disable the
          // functionality that depends on this permission.
        }
        return;
      }
    }
  }


}
