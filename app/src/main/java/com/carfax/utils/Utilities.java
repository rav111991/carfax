package com.carfax.utils;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;

import com.carfax.Model.Dealer;
import com.carfax.Model.Listings;

public class Utilities {

  public static final int MY_PERMISSIONS_REQUEST_CALL = 101;
  public static ProgressDialog mProgressDialog;
  /**
   * Used to create a location string from multiple strings
   * @param dealer
   * @return
   */
  public static String getLocation(Dealer dealer) {
    StringBuilder location = new StringBuilder();
    if (dealer.getAddress() != null && !dealer.getAddress().isEmpty())
      location = location.append(dealer.getAddress());

    if (location.length() > 0)
      location.append(", ");

    if (dealer.getCity() != null && !dealer.getCity().isEmpty())

      location = location.append(dealer.getCity());

    if (location.length() > 0)
      location.append(", ");

    if (dealer.getState() != null && !dealer.getState().isEmpty())
      location = location.append(dealer.getState());

    return location.toString();

  }


  public static String getName(Listings listings) {
    StringBuilder details = new StringBuilder();
    if (listings.getYear() != null && !listings.getYear().isEmpty())
      details = details.append(listings.getYear());

    if (listings.getMake() != null && !listings.getMake().isEmpty())
      details = details.append(", " + listings.getMake());

    if (listings.getModel() != null && !listings.getModel().isEmpty())
      details = details.append(", " + listings.getModel());

    if (listings.getTrim() != null && !listings.getTrim().isEmpty())
      details = details.append(", " + listings.getTrim());

    return details.toString();
  }

  /**
   * Check weather the String is valid
   * @param value
   * @return String
   */
  public static boolean isStringEmpty(String value) {
    if (value != null && !value.isEmpty())
      return false;
    else
      return true;
  }

  /**
   * check for run time permissions
   * @param activity
   */
  public static void checkPermission(Activity activity) {


      // request the permission
      ActivityCompat.requestPermissions(activity,
          new String[]{Manifest.permission.CALL_PHONE},
          MY_PERMISSIONS_REQUEST_CALL);



  }

// convert and return miles to format
  public static String changeToMiles(String miles) {
    double milesDouble = Double.parseDouble(miles) / 1000;

    return String.format("%.1f", milesDouble) + "k Mi.";

  }

  public static void callDealer(String number, Activity activity) {
    Intent intent = new Intent(Intent.ACTION_CALL,
        Uri.parse("tel:" + number));
    activity.startActivity(intent);
  }


}
