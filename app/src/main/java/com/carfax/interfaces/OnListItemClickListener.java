package com.carfax.interfaces;

import android.view.View;

public interface OnListItemClickListener {
  public void itemClicked(View view, int position);
}
