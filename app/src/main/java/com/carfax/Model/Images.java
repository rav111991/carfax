package com.carfax.Model;

import java.io.Serializable;

public class Images implements Serializable {

  private String[] large;
  private String[] medium;
  private String[] small;

  public String[] getLarge ()
  {
    return large;
  }

  public void setLarge (String[] large)
  {
    this.large = large;
  }

  public String[] getMedium ()
  {
    return medium;
  }

  public void setMedium (String[] medium)
  {
    this.medium = medium;
  }
  public String[] getSmall ()
  {
    return small;
  }

  public void setSmall (String[] small)
  {
    this.small = small;
  }

 
}
