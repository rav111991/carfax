package com.carfax.Model;

import java.io.Serializable;

public class Listings implements Serializable {
  private String model;
  private String mileage;
  private Images images;
  private String make;
  private String currentPrice;
  private String listPrice;
  private String imageCount;
  private String trim;
  private String year;
  private Dealer dealer;
  private String exteriorColor;
  private String interiorColor;
  private String drivetype;
  private String transmission;
  private String engine;
  private String bodytype;
  private String fuel;

  public String getFuel() {
    return fuel;
  }

  public void setFuel(String fuel) {
    this.fuel = fuel;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getMileage() {
    return mileage;
  }

  public void setMileage(String mileage) {
    this.mileage = mileage;
  }

  public Images getImages() {
    return images;
  }

  public void setImages(Images images) {
    this.images = images;
  }

  public String getMake() {
    return make;
  }

  public void setMake(String make) {
    this.make = make;
  }

  public String getCurrentPrice() {
    return currentPrice;
  }

  public void setCurrentPrice(String currentPrice) {
    this.currentPrice = currentPrice;
  }

  public String getListPrice() {
    return listPrice;
  }

  public void setListPrice(String listPrice) {
    this.listPrice = listPrice;
  }

  public String getTrim() {
    return trim;
  }

  public void setTrim(String trim) {
    this.trim = trim;
  }

  public Dealer getDealer() {
    return dealer;
  }

  public void setDealer(Dealer dealer) {
    this.dealer = dealer;
  }

  public String getExteriorColor() {
    return exteriorColor;
  }

  public void setExteriorColor(String exteriorColor) {
    this.exteriorColor = exteriorColor;
  }

  public String getInteriorColor() {
    return interiorColor;
  }

  public void setInteriorColor(String interiorColor) {
    this.interiorColor = interiorColor;
  }

  public String getDrivetype() {
    return drivetype;
  }

  public void setDrivetype(String drivetype) {
    this.drivetype = drivetype;
  }

  public String getEngine() {
    return engine;
  }

  public void setEngine(String engine) {
    this.engine = engine;
  }

  public String getBodytype() {
    return bodytype;
  }

  public void setBodytype(String bodytype) {
    this.bodytype = bodytype;
  }

  public String getTransmission ()
  {
    return transmission;
  }

  public void setTransmission (String transmission)
  {
    this.transmission = transmission;
  }

  public String getImageCount ()
  {
    return imageCount;
  }
  public void setImageCount (String imageCount)
  {
    this.imageCount = imageCount;
  }
  public String getYear ()
  {
    return year;
  }

  public void setYear (String year)
  {
    this.year = year;
  }
}
