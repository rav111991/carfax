package com.carfax.Model;

import java.io.Serializable;

public class CarDataModel implements Serializable {

  private Listings[] listings;

  public Listings[] getListings() {
    return listings;
  }

  public void setListings(Listings[] listings) {
    this.listings = listings;
  }
}
