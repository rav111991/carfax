package com.carfax.connections;

import com.carfax.Model.CarDataModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ServerConnectionAPI {
  @GET("assignment.json")
  Call<CarDataModel> getCarDetail();
}
