package com.carfax.connections;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

 public class APIClient {
  private static Retrofit retrofit = null;

   /**
    * method is used to initialize retrofit client
    * @return
    */
   public static Retrofit getClient() {


    retrofit = new Retrofit.Builder()
        .baseUrl("https://carfax-for-consumers.firebaseio.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build();



    return retrofit;
  }
}
