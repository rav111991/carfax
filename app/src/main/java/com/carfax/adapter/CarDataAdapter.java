package com.carfax.adapter;

import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.carfax.MainActivity;
import com.carfax.Model.CarDataModel;
import com.carfax.Model.Dealer;
import com.carfax.Model.Listings;
import com.carfax.R;
import com.carfax.interfaces.OnListItemClickListener;
import com.carfax.utils.Utilities;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarDataAdapter extends RecyclerView.Adapter<CarDataAdapter.ViewHolder> implements View.OnClickListener {


  private  Listings[] carList;
  private  OnListItemClickListener clickLIstener;

  public CarDataAdapter(Listings[] carList, OnListItemClickListener onListItemClickListener) {
    this.carList = carList;
    this.clickLIstener = onListItemClickListener;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    // create a new view
    LayoutInflater inflater = LayoutInflater.from(
        viewGroup.getContext());
    View v = inflater.inflate(R.layout.car_item_layout, viewGroup, false);
    ViewHolder viewHolder = new ViewHolder(v);
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

    Listings carDetail = carList[i];
    Dealer dealerDetail = carDetail.getDealer();

    viewHolder.callButton.setTag(i);
    viewHolder.layout.setTag(i);
    viewHolder.locationTextview.setText(Utilities.getLocation(dealerDetail));
    viewHolder.nameTextview.setText(Utilities.getName(carDetail));
    if(!Utilities.isStringEmpty(carDetail.getMileage()))viewHolder.mileageTextview.setText(Utilities.changeToMiles(carDetail.getMileage()));

    if(!Utilities.isStringEmpty(carDetail.getCurrentPrice()))viewHolder.priceTextview.setText(
        "$"+carDetail.getCurrentPrice());

   if(carDetail.getImages()!=null && carDetail.getImages().getLarge().length>0 && !Utilities.isStringEmpty(carDetail.getImages().getLarge()[0]))
   {
     String url = carDetail.getImages().getLarge()[0];
     Picasso.get().load(url).fit()
         .centerCrop().into(viewHolder.carImageview);
   }


    //viewHolder.mileageTextview.setText(carDetail.getMileage());
    viewHolder.callButton.setOnClickListener(CarDataAdapter.this);
    viewHolder.layout.setOnClickListener(CarDataAdapter.this);
  }

  @Override
  public int getItemCount() {
    return carList.length;
  }

  @Override
  public void onClick(View view) {
   int position =  Integer.parseInt(view.getTag().toString());

      clickLIstener.itemClicked(view, position);

  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    // each data item is just a string in this case
    @BindView(R.id.car_imageview)
    ImageView carImageview;
    @BindView(R.id.name_textview)
    TextView nameTextview;
    @BindView(R.id.price_textview)
    TextView priceTextview;
    @BindView(R.id.mileage_textview)
    TextView mileageTextview;
    @BindView(R.id.location_textview)
    TextView locationTextview;
    @BindView(R.id.call_button)
    Button callButton;

    public View layout;

    public ViewHolder(View view) {
      super(view);
      layout = view;
      ButterKnife.bind(this, view);

    }

  }
}
